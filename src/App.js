import React from 'react';
import './App.css';
import AverageWeightApp from './components/AverageWeightApp';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
      <AverageWeightApp />
  );
}

export default App;
