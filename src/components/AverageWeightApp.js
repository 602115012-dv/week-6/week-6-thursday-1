import React from 'react';
import Input from './Input';
import Person from './entity/Person';
import PersonList from './PersonList';
import Summary from './Summary';
import ClearButton from './ClearButton';

class AverageWeightApp extends React.Component {
  state = {
    key: 1,
    persons: []
  }

  render() {
    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-4">
            <Input addPerson={this.addPerson} />
            <PersonList persons={this.state.persons}/>
          </div>
          <div className="w-100"></div>
          <div className="col-4">
            <Summary persons={this.state.persons}/>
          </div>
          <div className="w-100"></div>
          <div className="col-4">
            <ClearButton clearData={this.clearData}/>
          </div>
        </div>
      </div>
    );
  }

  addPerson = (name, weight) => {
    let persons = this.state.persons;
    persons.push(new Person(this.state.key, name, weight));
    this.setState({
      key: this.state.key + 1,
      persons: persons
    });
  }

  clearData = () => {
    this.setState({
      persons: []
    });
  }
}

export default AverageWeightApp;