import React from 'react';

class PersonList extends React.Component {
  render() {
    console.log("renderTableData", this.renderTableData);
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Weight</th>
          </tr>
        </thead>
        <tbody>
          {this.renderTableData()}
        </tbody>
      </table>
    );
  }

  renderTableData = () => {
   return this.props.persons.map((person) => {
      return (
        <tr key={person.key}>
          <td>{person.key}</td>
          <td>{person.name}</td>
          <td>{person.weight}</td>
        </tr>
      );
    })
  }

}

export default PersonList;