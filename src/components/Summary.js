import React from 'react';

class Summary extends React.Component {
  render() {
    return(
      <div style={{textAlign: "center"}}>
        The average is {isNaN(this.getAverage()) ? '' : this.getAverage()}
      </div>
    );
  }

  getAverage = () => {
    let sum = 0;
    this.props.persons.forEach(element => {
      sum += parseInt(element.weight);
    });
    return sum / this.props.persons.length
  }
}

export default Summary;