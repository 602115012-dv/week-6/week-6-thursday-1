class Person {
  constructor(key, name, weight) {
    this.key = key;
    this.name = name;
    this.weight = weight;
  }
}

export default Person;